# Testing with Cypress Blog

A Next.js Blog .

## Installation

```bash
yarn install
```

Start the local dev server.

```bash
yarn dev
```

While the dev server is running, in a separate terminal window, run the following command to launch Cypress.

```bash
yarn cypress:open
```


This repo is a fork of the [Next.js Learn Starter](https://github.com/vercel/next-learn-starter/)
