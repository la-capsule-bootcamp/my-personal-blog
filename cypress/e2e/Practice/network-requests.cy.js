/*
  It is very helpful to use console.log() to log out the response data
  in order to see the data you are working with.
  
*/
describe("Network Requests", () => {
  // Do not change beforeEach() content
  beforeEach(() => {
    cy.intercept("GET", "http://localhost:3000/api/posts", (req) => {
      // this is to disable browser caching
      delete req.headers["if-none-match"];
    }).as("posts");

    cy.visit("http://localhost:3000");
  });

  // Start coding here :)
  it("/api/posts returns a status code of 200", () => {
    // Write an assertion that the route '/api/posts'
    // returns a status code of 200
    // Hint: You will need to use cy.request()
    // https://docs.cypress.io/api/commands/request
  });

  it("/api/posts returns the correct number of posts", () => {
    // Write an assertion that the route '/api/posts'
    // returns the correct number of posts.
  });
});
